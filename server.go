package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type Response struct {
	Status int    `json:"status"`
	Path   string `json:"path"`
}

func createDirIfNotExist(dir string) {
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}

func storeUploadedFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	authHeader, ok := auth(r)
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer file.Close()
	filePath := "storage/" + handler.Filename
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header["Content-Type"])
	fileToSave, err := os.Create(filePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer fileToSave.Close()
	_, err = io.Copy(fileToSave, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("Storing metadata on other service")
	// Store metadata on another service
	client := &http.Client{}
	jsonStr := []byte(fmt.Sprintf(
		`{"filename":"%s","size":%d,"mimetype":"%s"}`,
		handler.Filename, handler.Size, handler.Header["Content-Type"],
	))
	log.Println(string(jsonStr))
	log.Println("Request")
	req, err := http.NewRequest("POST", "127.0.0.1:20104/", bytes.NewBuffer(jsonStr))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("1")
	req.Header.Set("Authorization", authHeader)
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	log.Println("2")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("3")
	if resp.StatusCode == 201 {
		response := Response{200, r.Host + "/" + filePath}
		js, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
		return
	}

}

func auth(r *http.Request) (string, bool) {
	auth := r.Header.Get("Authorization")
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://oauth.infralabs.cs.ui.ac.id/oauth/resource", nil)
	if err != nil {
		return "", false
	}

	req.Header.Set("Authorization", auth)
	resp, err := client.Do(req)
	if err != nil {
		return "", false
	}

	if resp.StatusCode == 200 {
		return auth, true
	}
	return "", false
}

func main() {
	createDirIfNotExist("storage")
	log.Println("Server is listening on port 20103")
	http.HandleFunc("/upload", storeUploadedFile)
	http.Handle("/storage/", http.FileServer(http.Dir(".")))
	http.ListenAndServe(":20103", nil)
}
